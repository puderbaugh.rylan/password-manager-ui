import { createRouter, createWebHistory } from 'vue-router';

import HomeView from '../views/HomeView.vue';
import LoginView from '../views/LoginView.vue';
import VaultView from '../views/Vault.vue';

import RegisterView from '../views/RegisterView.vue';
import CredentialsSaveView from "../views/VaultSaveView.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        { path: '/', name: 'home', component: HomeView },
        { path: '/login', name: 'login', component: LoginView },
        { path: '/register', name: 'register', component: RegisterView },
        { path: '/credentials/save', name: 'save', component: CredentialsSaveView },
        { path: '/credentials/retrieve', name: 'vault', component: VaultView },
    ],
});

export default router;